#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <random>
#include <time.h>
#include <vector>
#include <windows.h>
#include <stdlib.h>
#include "Equipo.h"
#include "Matrix.h"
#include "Game.h"
#include "Season.h"

using namespace std;

class Tournament{
public:
	Tournament(){
		this->name = "New Tournament";
		this->size = 0;
		this->type = false;
		this->win = 0;
		this->lose = 0;
		this->tie = 0;
		this->teams = new Equipo[0];
		this->seasons = new Season[0];
		this->currYear = 2014;
	}

	Tournament(string nam, int size, bool type, int win, int lose, int tie, Equipo *teams)
	{
		this->name = nam;
		this->size = size;
		this->type = type;
		this->win = win;
		this->lose = lose;
		this->tie = tie;
		this->teams = teams;
		this->currYear = 2014;
	}

	Tournament(string fileName){
		this->currYear = 2014;
		this->organization = new Matrix();

		//this->teams = new Equipo[20];
		//this->dates = new Fecha[20];
		this->name = fileName;
		this->size = 0;
		vector<Equipo> tempTeams;
		vector<Fecha> tempDates;
		
		ifstream ifs;
		ifs.open(fileName.c_str());
		string buffer;
		string tempname;
		ifs >> buffer;
		while (buffer != "//"){
			tempname += buffer + " ";
			ifs >> buffer;
		}
		buffer = "";
		this->name = tempname;
		ifs >> this->type;
		ifs >> this->win;
		ifs >> this->tie;
		ifs >> this->lose;
		int tempWeight = 0;
		ifs >> buffer;
		while (buffer != "//"){
			tempname = "";
			while (buffer != "/"){
				tempname += buffer + " ";
				ifs >> buffer;
			}
			ifs >> tempWeight;
			tempTeams.push_back(Equipo(tempname, tempWeight));
			//this->teams[this->size] = Equipo(tempname, tempWeight);
			ifs >> tempname;
			tempTeams.at(this->size).setAbrev(tempname);
			//this->teams[this->size].setAbrev(tempname);
			tempDates.push_back(Fecha(this->size+1, 06, 2014));
			this->size++;
			ifs >> buffer;
		}
		this->maxSize = 0;
		if (tempTeams.size()%2==0){
			this->maxSize = tempTeams.size();
		}
		else{
			this->maxSize = this->organization->nextPowOf2(tempTeams.size());
		}
		//cout << "Cantidad de Equipos: " << tempTeams.size() << endl;
		//cout << "Tama�o Total: " << this->maxSize << endl;
		this->teams = new Equipo[this->maxSize];
		this->dates = new Fecha[this->maxSize];
		this->organization->setRoundRobin(this->maxSize);
		
		for (int i = 0; i < this->maxSize; i++){
			int taman = (tempTeams.size());
			if (i<taman){
				teams[i] = tempTeams.at(i);
			}
			else{
				teams[i] = Equipo("Descanso", 0);
				teams[i].setAbrev("FFF");
			}
			this->dates[i] = tempDates.at(i);
		}
		//this->organization->print();
		//cout << tempname << endl;
	}

	Tournament(string fileName, string Title){
		this->teams = new Equipo[20];
		this->name = Title;
		//int i = 0;
		ifstream ifs;
		ifs.open(fileName.c_str());
		//string tempname = "";
		string currString;
		string currString2;
		ifs >> currString;
		ifs >> currString2;
		while (currString != "/"){
			//tempname += currString + " ";
			this->teams[this->size++] = Equipo(currString, stoi(currString2));
			ifs >> currString;
			ifs >> currString2;

		}
		//cout << tempname << endl;
	}

	int  getSize(){
		return this->size;
	}

	string  getName(){
		return this->name;
	}
	bool  getType(){
		return this->type;
	}
	int  getWin(){
		return this->win;
	}
	int  getLose(){
		return this->lose;
	}
	int  getTie(){
		return this->tie;
	}
	Equipo *  getTeams(){
		return this->teams;
	}

	Equipo getTeam(int ind){
		return this->teams[ind];
	}
	void  setName(string nam){
		this->name = nam;
	}
	void  setSize(int size){
		this->size = size;
	}
	void  setType(bool type){
		this->type = type;
	}
	void  setWin(int pts){
		this->win = pts;
	}
	void  setLose(int pts){
		this->lose = pts;
	}
	void  setTie(int pts){
		this->tie = pts;
	}
	void  replaceTeams(Equipo *teams){
		this->teams = teams;
	}

	void  print(){
		cout <<"Nombre del Torneo: "<< this->name << endl;
		cout << "Cantidad de Equipos: " << this->size << endl;
		//cout <<"Tipo de Torneo: "<< this->type << endl;
		//cout <<"Ganados: "<< this->win << endl;
		//cout <<"Perdidos: "<< this->lose << endl;
		//cout << "Empates: " << this->tie << endl;
		cout << "Reglas: " << endl;
		cout << "Al ganador de un partido se le otorgar�n 3 puntos" << endl;
		cout << "Un empate har� que cada uno de los equipos obtenga 1 punto" << endl;
		cout << "Si hay un empate en puntuaci�n, el ganador del torneo se decidir� a trav�s de la cantidad de goles a favor" << endl;
		cout << "Fecha Inicial: "; 
		this->dates[0].print();
		cout << "Equipos: " << endl;
		for (int i = 0; i < this->size; i++){
			this->teams[i].print();
		}

	}
	void  printTeams(){
		cout << "Equipos participantes: ";
		this->dates[0].print();
		cout << "Equipos: " << endl;
		for (int i = 0; i < this->size; i++){
			this->teams[i].print();
		}

	}

	void  printPeso(){
		cout << "Reporte de equipos con peso: " << endl;
		for (int i = 0; i < this->size; i++){
			this->teams[i].printConPeso();
		}
	}

	Game simularPartido(Equipo  &a, Equipo  &b, Fecha &fecha){
		int vA = 0;
		int vB = 0;
		srand(time(NULL));
		vA = a.getWeight()*(rand() % 5 +1);
		vB = b.getWeight()*(rand() % 5 +1);
		vA = vA % 6;
		vB = vB % 6;
		if (vA > vB){
			cout << a.getAbrev() << " vs " << b.getAbrev() << endl;
			cout << vA << "       " << vB << endl;
			if (b.getAbrev()!="FFF"){
				a.addWin();
				a.addGoals(vA);
				b.addGoals(vB);
				b.recieveGoals(vA);
				a.recieveGoals(vB);
				b.addLose();
			}
		}
		else if (vB > vA){
			cout << a.getAbrev() << " vs " << b.getAbrev() << endl;
			cout << vA%6 << "       " << vB%6 << endl;
			if (b.getAbrev()!="FFF"){
				b.addWin();
				a.addGoals(vA);
				b.addGoals(vB);
				b.recieveGoals(vA);
				a.recieveGoals(vB);
				a.addLose();
			}
		}
		else if (vB == vA){
			//cout << "Empate" << endl;
			cout << a.getAbrev() << " vs " << b.getAbrev() << endl;
			cout << vA % 6 << "       " << vB % 6 << endl;
			//cout << "A" << endl;
			a.addTie();
			//cout << "B" << endl;
			a.addGoals(vA);
			//cout << "C" << endl;
			b.addGoals(vB);
			//cout << "D" << endl;
			b.recieveGoals(vA);
			//cout << "E" << endl;
			a.recieveGoals(vB);
			//cout << "F" << endl;
			b.addTie();
		}
		Game newgame = Game(a, b, vA, vB, fecha);
		return newgame;
	}

	


	Equipo getWinner(){
		this->winner = this->teams[0];
		for (int i = 1; i < this->size; i++){
			if (winner.obtenerPuntuacion() < this->teams[i].obtenerPuntuacion()){
				this->winner = this->teams[i];
			}
			else if (this->winner.obtenerPuntuacion() == this->teams[i].obtenerPuntuacion()){
				if (winner.getGoals() < this->teams[i].getGoals()){
					this->winner = this->teams[i];
				}
			}
		}
		return this->winner;
	}
	/*void printGames(){
		for (int i = 0; i < this->gamesno; i++){
			this->games[i].printResultado();
		}
	}*/
	void mainMenu(){
		bool end = false;
		bool regresar = false;
		string accion;
		cout << endl;
		cout << " ..       ..                                  " << endl;
		cout << "|  |||||||  |    //////       ////// |||   ||| |||||||  " << endl;
		cout << "|..|||||||..|  ///          ///      |||   ||| |||  ||  " << endl;
		cout << "    |||||      ///     |||| ///      |||   ||| |||||||  " << endl;
		cout << "      |        ///          ///      |||   ||| |||     " << endl;
		cout << "     |||         //////       //////   |||||   |||      " << endl;
		cout << endl;
		cout << "    The C++ soccer simulator!" << endl;
		Beep(349, 100);
		Beep(349, 100);
		Beep(440, 200);
		Beep(440, 80);
		Beep(440, 80);
		Beep(440, 80);
		Beep(523, 100);
		Beep(440, 100);
		Beep(659, 1000);
		cout << endl;
		while (!end){

			cout << "Ingrese una letra para comenzar" << endl;
			cout << endl;
			cout << "C - Iniciar un nuevo torneo.     S - Mostrar estadisticas." << endl;
			cout << "H - Celebraci�n.                 P - Imprimir datos del Torneo" << endl;
			cout << "                                 E - Salir" << endl;
			cin >> accion;
			if (accion == "E" || accion == "e"){
				end = true;
			}
			else if (accion == "P" || accion == "p"){
				this->print();
			}
			else if (accion == "C" || accion == "c"){
				cout << endl;
				cout << "A - Torneo Automatico     P - Torneo jornada por jornada" << endl;
				string modo = "";
				while (modo != "A" && modo != "a" && modo != "P" && modo != "p"){
				cout << "Escriba su eleccion: " << endl;
				cin >> modo;
				cout << modo << endl;
				}
				bool pasoApaso;
				if (modo == "A" || modo =="a"){
					pasoApaso = false;
				}
				else{
					pasoApaso = true;
				}
				this->simularTorneo(pasoApaso);
			}
			else if (accion == "S" || accion == "s"){
				while (!regresar){
					cout << endl;
					cout << "La base de datos contiene " << this->seasonsno << " torneos." << endl;
					bool valid = false;
					if (seasonsno > 1){
						while (!valid&&!regresar){
							cout << "Torneos " << this->seasons[0].getYear() << " - ";
							cout << this->seasons[this->seasonsno - 1].getYear() << endl;
							cout << "Escriba el a�o del torneo que quiere ver o 'R' para regresar" << endl;
							cin >> accion;
							if (accion == "R" || accion == "r"){
								regresar = true;
							}
							else{
								int anio = stoi(accion) - this->seasons[0].getYear();
								if (anio >= 0 && anio < this->seasonsno){
									imprimeEstadisticas(anio);
								}
								else{
									cout << "Anio invalido. Ingrese un numero entre " << this->seasons[0].getYear() << " y ";
									cout << this->seasons[this->seasonsno - 1].getYear() << endl;
								}
							}
						}
						regresar = false;
					}
					else if (seasonsno == 1){
						cout << "Torneo " << this->seasons[0].getYear() << endl;
						cout << "�Ver el reporte? (S/N)" << endl;
						cin >> accion;
						if (accion == "S" || accion == "s"){
							cout << "Deseas escribirlo en un archivo (S/N): ";
							cin >> accion;
							if (accion == "S" || accion == "s"){
								writeStats(0);

							}
							else{
								imprimeEstadisticas(0);
								
							}
							
						}
					}
					else{
						cout << "Inicie un torneo para alimentar la base de datos." << endl;
					}

					cout << endl;
					cout << "�Desea realizar otra consulta? (S/N)" << endl;
					cin >> accion;
					if (accion == "N" || accion == "n"){
						regresar = true;
						cout << "__________________________________________________________" << endl;
					}
				}
				regresar = false;

			}
			else if (accion == "H" || accion =="h"){
					Beep (330,100);
					Beep (330,300);
					Beep (330,300);
					Beep (262,100);
					Beep (330,300);
					Beep (392,700);
					Beep (196,700);
					Beep (196,125);
					Beep (262,125);
					Beep (330,125);
					Beep (392,125);
					Beep (523,125);
					Beep (660,125);
					Beep (784,575);
					Beep (660,575);
					Beep (207,125);
					Beep (262,125);
					Beep (311,125);
					Beep (415,125);
					Beep (523,125);
					Beep (622,125);
					Beep (830,575);
					Beep (622,575);
					Beep (233,125);
					Beep (294,125);
					Beep (349,125);
					Beep (466,125);
					Beep (587,125);
					Beep (698,125);
					Beep (932,575);
					Beep (932,125);
					Beep (932,125);
					Beep (932,125);
					Beep (1046,675);
			}
			else{
				cout << "Comando invalido" << endl;
				cout << endl;
			}
		}

	}

	void simularTorneo(bool steps){
		Beep(330, 80);
		Beep(494, 400);
		vector<Game> juegostemp;		
		for (int i = 1; i < this->organization->getRows(); i++){
			Beep(330, 80);
			cout << "---------------" << endl;
			this->dates[i-1].print();
			for (int j = 0; j < this->organization->getColumns()-2; j+=2){
				int coord = (int)this->organization->getElement(i - 1, j) ;
				int coord2 = (int)this->organization->getElement(i - 1, j + 1);
				coord = coord%this->maxSize;
				coord2 = coord2%this->maxSize;
				if (this->teams[coord].getAbrev() != this->teams[coord2].getAbrev()){
					Game newgame = this->simularPartido(this->teams[coord], this->teams[coord2], this->dates[i - 1]);
					juegostemp.push_back(newgame);
					cout << endl;
					
				}
			}
			if (steps){
				getchar();
			}
		}
		int taman = juegostemp.size();
		Game *temporadaactual = new Game[taman];
		for (int i = 0; i < (int)(juegostemp.size()); i++){
			temporadaactual[i] = juegostemp.at(i);
		}
		Equipo * aOrganizar = this->teams;

		this->quickSort(aOrganizar, this->size);
		this->teams = aOrganizar;
		Season nuevoSeason = Season(temporadaactual, juegostemp.size(), this->getTeams(),
									this->getSize(), this->currYear++);

		//int sizethisSeason = juegostemp.size();
		this->addSeason(nuevoSeason);

		this->seasons[this->seasonsno-1].printStats();

		cout << endl;
		cout << " ..       ..    " << endl;
		cout << "|  |||||||  |  " << endl;
		cout << "|..|||||||..| " << endl;
		cout << "    |||||        " << endl;
		cout << "      |      " << endl;
		cout << "     |||        " << endl;
		cout << endl;
		cout << "El ganador es: " << endl;
		getWinner().print();
		cout << endl;
		Beep(659, 300);
		Beep(659, 100);
		Beep(659, 80);
		Beep(659, 1000);
		cout << "CURR YEAR" << currYear<< endl;
	}
	void addSeason(Season seas){
		if (seasonsno == seasonssize){
			int prevseasons = this->seasonssize;
			this->seasonssize += 3;
			Season * largerSeasons = new Season[this->seasonssize];
			for (int i = 0; i < prevseasons; i++){
				largerSeasons[i] = this->seasons[i];
			}
			this->seasons = largerSeasons;
		}
		this->seasons[this->seasonsno++] = seas;
	}
	void imprimeEstadisticas(int temporada){
		cout << "Imprimiendo estadisticas de la temporada " << this->seasons[temporada].getYear() <<"."<< endl;
		cout << endl;
		cout << "________________________________" << endl;
		cout << "Elija el criterio de ordenaci�n:" << endl;
		cout << "PTS - Orden por puntos     DIF - Orden por diferencia de goles" << endl;
		cout << "GF - Goles a favor" << endl;
		string accion;
		Equipo * aOrganizar = this->seasons[temporada].getTeams();
		bool correctInput = false;
		while (!correctInput){
			cin >> accion;
			if (accion == "PTS" || accion == "pts"){
				cout << "Mostrando estadisticas por lugar en el campeonato..." << endl;
				cout << endl;
				this->quickSort(aOrganizar, this->size);
				this->teams = aOrganizar;
				correctInput = true;
			}
			else if (accion == "DIF" || accion == "dif"){
				cout << "Mostrando estadisticas por diferencia de goles..." << endl;
				cout << endl;
				this->quickSortDIF(aOrganizar, this->size);
				this->teams = aOrganizar;
				correctInput = true;
			}
			else if (accion == "GF" || accion == "gf"){
				cout << "Mostrando estadisticas por goles a favor..." << endl;
				cout << endl;
				this->quickSortGF(aOrganizar, this->size);
				this->teams = aOrganizar;
				correctInput = true;
			}
			if (correctInput){
				this->seasons[temporada].setTeams(aOrganizar);
				this->seasons[temporada].printStats();
			}
		}
		cout << "________________________________" << endl;
	}

	void writeStats(int temporada){
		cout << "Imprimiendo estadisticas de la temporada " << this->seasons[temporada].getYear() << "." << endl;
		cout << endl;
		cout << "________________________________" << endl;
		cout << "Elija el criterio de ordenaci�n:" << endl;
		cout << "PTS - Orden por puntos     DIF - Orden por diferencia de goles" << endl;
		cout << "GF - Goles a favor" << endl;
		string accion;
		Equipo * aOrganizar = this->seasons[temporada].getTeams();
		bool correctInput = false;
		while (!correctInput){
			cin >> accion;
			if (accion == "PTS" || accion == "pts"){
				cout << "Mostrando estadisticas por lugar en el campeonato..." << endl;
				cout << endl;
				this->quickSort(aOrganizar, this->size);
				this->teams = aOrganizar;
				correctInput = true;
			}
			else if (accion == "DIF" || accion == "dif"){
				cout << "Mostrando estadisticas por diferencia de goles..." << endl;
				cout << endl;
				this->quickSortDIF(aOrganizar, this->size);
				this->teams = aOrganizar;
				correctInput = true;
			}
			else if (accion == "GF" || accion == "gf"){
				cout << "Mostrando estadisticas por goles a favor..." << endl;
				cout << endl;
				this->quickSortGF(aOrganizar, this->size);
				this->teams = aOrganizar;
				correctInput = true;
			}
			if (correctInput){
				this->seasons[temporada].setTeams(aOrganizar);
				this->seasons[temporada].writeStats();
			}
		}
		cout << "________________________________" << endl;
	}


	//Ordenar equipos por lugar
	void swap(Equipo *list, int i, int j)
	{
		Equipo temp = list[i];
		list[i] = list[j];
		list[j] = temp;
	}
	int partition(Equipo *a, int begin, int end)
	{
		int piv = begin;
		int i = begin + 1;
		int j = end;
		while (i <= j)
		{
			while (i <= j && a[i].obtenerPuntuacion() > a[piv].obtenerPuntuacion())
			{
				i++;
			}
			while (i <= j && a[j].obtenerPuntuacion() <= a[piv].obtenerPuntuacion())
			{
				j--;
			}
			if (i <= j)
			{
				swap(a, i, j);
				i++;
				j--;
			}
		}
		swap(a, piv, j);
		return j;
	}

	void quickSort(Equipo *a, int begin, int end)
	{
		if (begin < end)
		{
			int k = partition(a, begin, end);
			quickSort(a, begin, k - 1);
			quickSort(a, k + 1, end);
		}
	}

	void quickSort(Equipo *a, int n)
	{
		quickSort(a, 0, n - 1);
	}
	//Ordenar los equipos por diferencia de goles
	int partitionDIF(Equipo *a, int begin, int end)
	{
		int piv = begin;
		int i = begin + 1;
		int j = end;
		while (i <= j)
		{
			while (i <= j && a[i].getGoalsDifference() > a[piv].getGoalsDifference())
			{
				i++;
			}
			while (i <= j && a[j].getGoalsDifference()<= a[piv].getGoalsDifference())
			{
				j--;
			}
			if (i <= j)
			{
				swap(a, i, j);
				i++;
				j--;
			}
		}
		swap(a, piv, j);
		return j;
	}

	void quickSortDIF(Equipo *a, int begin, int end)
	{
		if (begin < end)
		{
			int k = partitionDIF(a, begin, end);
			quickSortDIF(a, begin, k - 1);
			quickSortDIF(a, k + 1, end);
		}
	}

	void quickSortDIF(Equipo *a, int n)
	{
		quickSortDIF(a, 0, n - 1);
	}
	//por Goles a favor
	int partitionGF(Equipo *a, int begin, int end)
	{
		int piv = begin;
		int i = begin + 1;
		int j = end;
		while (i <= j)
		{
			while (i <= j && a[i].getGoals() > a[piv].getGoals())
			{
				i++;
			}
			while (i <= j && a[j].getGoals() <= a[piv].getGoals())
			{
				j--;
			}
			if (i <= j)
			{
				swap(a, i, j);
				i++;
				j--;
			}
		}
		swap(a, piv, j);
		return j;
	}

	void quickSortGF(Equipo *a, int begin, int end)
	{
		if (begin < end)
		{
			int k = partitionGF(a, begin, end);
			quickSortGF(a, begin, k - 1);
			quickSortGF(a, k + 1, end);
		}
	}

	void quickSortGF(Equipo *a, int n)
	{
		quickSortGF(a, 0, n - 1);
	}
private:
	string name;
	int currYear;
	int size; //number of teams in the tournament.
	int seasonsno; //number of seasons.
	int seasonssize; //space in the season array
	int maxSize;
	bool type; //simple(false) or double(true) roundrobin
	int win, lose, tie; //points given to teams when winning, loosing or in case of a tie;
	Equipo * teams; //Array of teams;
	Equipo winner;
	Fecha * dates; //Array of dates
	Matrix * organization;
	Season * seasons; //Array of games in the tournament
};