#include <iostream>

using namespace std;

class Fecha{
	public:
		Fecha() {
			this->day = 0;
			this->month = 0;
			this->year = 0;
		}

		Fecha(int day, int month, int year){
			this->day = day % 31;
			this->month = month % 12;
			this->year = year;
			if (this->month == 2){
				this->day = this->day % 28;
			}
		}
		void setDate(int day, int month, int year){
			this->day = day%31;
			this->month = month%12;
			this->year = year;
			if (this->month == 2){
				this->day = this->day % 28;
			}

		}
		Fecha * getDate(){
			return this;
		}

		void print(){
			cout << day << "/" << month << "/" << year << endl;
		}

		int getDay(){
			return this->day;
		}

		int getMonth(){
			return this->month;
		}

		int getYear(){
			return this->year;
		}

		void setDay(int d){
			if (this->month == 2){
				this->day = d % 28;
			}
			else{ 
				this->day = d % 31; 
			}
		}

		void setMonth(int d){
			this->month = d;
		}

		void setYear(int d){
			this->year = d;
		}
	private:
		int day;
		int month;
		int year;
};