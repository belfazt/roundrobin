#pragma once
#include <iostream>
#include <cmath>
#include "Matrix.h"
#include "Equipo.h"
#include "Fecha.h"
#include <fstream>
#include <string>
#include <stdlib.h>
using namespace std;

class Game{
private:
	int goles1, goles2;
	Equipo equipo1;
	Equipo equipo2;
	Fecha fecha;
	bool tie;
public:
	Game(){
		this->equipo1;
		this->equipo2;
		this->goles1;
		this->goles2;
		this->fecha;
	}

	Game(Equipo equipo1, Equipo equipo2, int goles1, int goles2, Fecha date){
		this->equipo1 = equipo1;
		this->equipo2 = equipo2;
		this->goles1 = goles1;
		this->goles2 = goles2;
		this->fecha = date;
	}

	bool tied(){
		return this->tie;
	}
	void setTie(bool tie){
		this->tie = tie;
	}
	void printResultado(){
		cout << this->fecha.getDate() << endl;
		cout << this->equipo1.getAbrev() << " vs " << this->equipo2.getAbrev();
		cout << goles1 << "  -  " << goles2;
	}

};
