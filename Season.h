#pragma once
#include <iostream>
#include <cmath>
#include "Matrix.h"
#include "Game.h"
#include "Equipo.h"
#include <fstream>
#include <string>
#include <stdlib.h>
using namespace std;

class Season{
private:
	int seasonSize;
	int yearSeason;
	Game * juegos;
	Equipo * equipos;
	int teamssize;

public:
	Season(){

	}
	Season(Game * juegos, int seasonSize, Equipo * equipos, int teamssize, int year){
		this->juegos = juegos;
		this->seasonSize = seasonSize;
		this->equipos = equipos;
		this->teamssize = teamssize;
		this->yearSeason = year;
	}
	int getYear(){
		return this->yearSeason;
	}
	Equipo * getTeams(){
		return this->equipos;
	}
	void setTeams(Equipo * equips){
		this->equipos = equips;
	}
	void displaySeason(){

		for (int i = 0; i < this->seasonSize; i++){

		}
	}
	void printStats(){
		
		
		cout << endl;
		cout << "_________________________________________________  " << endl;
		cout << "                                 " << endl;
		cout << "    ////// //////////  /////  //////////  //////   " << endl;
		cout << "  //          ///     //  //     ///    //      " << endl;
		cout << "   /////     ///     ///////    ///      /////    " << endl;
		cout << "       //   ///     //    //   ///          // " << endl;
		cout << " //////    ///     //     //  ///     //////   " << endl;
		cout << "_________________________________________________  " << endl;
		cout << endl;
		for (int i = 0; i < this->teamssize; i++){
			cout << "Posicion " << i+1 << endl; 
			this->equipos[i].printStats();
		}
	}

	void writeStats(){
		ofstream file;
		
		file.open("resultados.txt");
		cout << "Escribiendo archivo" << endl;
		//getchar();
		cout << endl;
		file << endl;
		cout << "_________________________________________________  " << endl;
		file << "_________________________________________________  " << endl;
		cout << "                                 " << endl;
		file << "                                 " << endl;
		cout << "    ////// //////////  /////  //////////  //////   " << endl;
		file << "    ////// //////////  /////  //////////  //////   " << endl;
		cout << "  //          ///     //  //     ///    //      " << endl;
		file << "  //          ///     //  //     ///    //      " << endl;
		cout << "   /////     ///     ///////    ///      /////    " << endl;
		file << "   /////     ///     ///////    ///      /////    " << endl;
		cout << "       //   ///     //    //   ///          // " << endl;
		file << "       //   ///     //    //   ///          // " << endl;
		cout << " //////    ///     //     //  ///     //////   " << endl;
		file << " //////    ///     //     //  ///     //////   " << endl;
		cout << "_________________________________________________  " << endl;
		file << "_________________________________________________  " << endl;
		cout << endl;
		file << endl;
		for (int i = 0; i < this->teamssize; i++){
			cout << "Posicion " << i + 1 << endl;
			file << "Posicion " << i + 1 << endl;
			//this->equipos[i].printStats();
			
			cout << this->equipos[i].getName() << endl;
			cout << "Juegos: " << this->equipos[i].getGames() << endl;
			cout << "Ganados : " << this->equipos[i].getWins() << "   Perdidos: " << this->equipos[i].getLoses() << "     Empatados: " << this->equipos[i].getTies() << endl;
			cout << "Goles a Favor:" << this->equipos[i].getGoals() << " Goles en contra: " << this->equipos[i].getGMinus() << endl;
			cout << "Diferencia de goles: " << this->equipos[i].getGoalsDifference() << endl;
			cout << "Puntuación Total: " << this->equipos[i].obtenerPuntuacion() << endl;
			cout << endl;

			file << this->equipos[i].getName() << endl;
			file << "Juegos: " << this->equipos[i].getGames() << endl;
			file << "Ganados : " << this->equipos[i].getWins() << "   Perdidos: " << this->equipos[i].getLoses() << "     Empatados: " << this->equipos[i].getTies() << endl;
			file << "Goles a Favor:" << this->equipos[i].getGoals() << " Goles en contra: " << this->equipos[i].getGMinus() << endl;
			file << "Diferencia de goles: " << this->equipos[i].getGoalsDifference() << endl;
			file << "Puntuación Total: " << this->equipos[i].obtenerPuntuacion() << endl;
			file << endl;
		}
		file.close();
	}
	
	void displayWinners(){

	}
};
