﻿#pragma once
#include <iostream>
#include <cmath>
#include "Matrix.h"
#include <fstream>
#include <string>
#include <stdlib.h>
using namespace std;

class Matrix{
private:
	int m;
	int n;
	double **e;
public:
	Matrix(){
		this->m = 1;
		this->n = 1;
		this->e = this->createMatrix(1, 1);
		this->e[0][0] = 0.0;
	}
	Matrix(int m, int n) //matriz ordnen m x n
	{
		this->m = m;
		this->n = n;
		this->e = this->createMatrix(m, n);
	}
	Matrix(int order) // matriz orden "order"
	{
		this->m = order;
		this->n = order;
		this->e = this->createMatrix(m, n);
	}
	Matrix(int m, int n, double **e) //matriz orden m x n y elementos por matriz dinamica e
	{
		this->m = m;
		this->n = n;
		this->e = this->createMatrix(m, n);
		for (int i = 0; i < m; i++)
		{
			for (int j = 0; j < n; j++)
			{
				this->e[i][j] = e[i][j];
			}
		}
	}
	Matrix(string fileName){
		ifstream ifs;
		ifs.open(fileName.c_str());
		int mm, nn;
		ifs >> mm;
		ifs >> nn;
		this->m = mm;
		this->n = nn;
		this->e = this->createMatrix(mm, nn);
		int temp;
		for (int i = 0; i < m; i++)
		{
			for (int j = 0; j < n; j++)
			{
				ifs >> temp;
				this->e[i][j] = temp;
			}
		}
	}
	Matrix(int order, double **e) //matriz cudadrada y elemento de matriz dinamica
	{
		this->m = order;
		this->n = order;
		this->e = this->createMatrix(order, order);
		for (int i = 0; i < order; i++)
		{
			for (int j = 0; j < order; j++)
			{
				this->e[i][j] = e[i][j];
			}
		}
	}
	Matrix(Matrix *b) //matriz copia
	{
		this->m = b->getRows();
		this->n = b->getColumns();
		this->e = this->createMatrix(m, n);
		for (int i = 0; i < m; i++)
		{
			for (int j = 0; j < n; j++)
			{
				this->e[i][j] = b->getElement(i, j);
			}
		}
	}
	~Matrix() //destructor matriz
	{
		for (int i = 0; i < m; i++)
		{
			delete[] e[i];
		}
		delete[]e;
	}
	double  getElement(int i, int j) //metodos analizadores o get
	{
		return this->e[i][j];
	}
	int  getRows()
	{
		return this->m;
	}
	int  getColumns()
	{
		return this->n;
	}
	void  setElement(int i, int j, double value) //metodos modificadores o set
	{
		this->e[i][j] = value;
	}
	//pre: m == n
	void  setIdentity()
	{
		for (int i = 0; i < this->m; i++)
		{
			for (int j = 0; j < this->n; j++)
			{
				if (i == j)
				{
					this->e[i][j] = 1.0;
				}
				else
				{
					this->e[i][j] = 0.0;
				}
			}
		}
	}
	void  setNull()
	{
		for (int i = 0; i < m; i++)
		{
			for (int j = 0; j < n; j++)
			{
				this->e[i][j] = 0.0;
			}
		}
	}
	//pre: mismo orden
	Matrix *  sum(Matrix *b) //suna de matrices
	{
		int m = b->getRows();
		int n = b->getColumns();
		Matrix *c = new Matrix(m, n);
		for (int i = 0; i < m; i++)
		{
			for (int j = 0; j < n; j++)
			{
				c->setElement(i, j, this->getElement(i, j) + b->getElement(i, j));
			}
		}
		return c;
	}
	void  print() //imprime matriz en salida std
	{
		for (int i = 0; i < m; i++)
		{
			for (int j = 0; j < n; j++)
			{
				cout << this->e[i][j] << " ";
			}
			cout << endl;
		}
	}

	bool  isIdentity(double err)
	{
		for (int i = 0; i < m; i++)
		{
			for (int j = 0; j < n; j++)
			{
				if (i != j){
					if (fabs(this->e[i][j]) > err){
						return false;
					}
				}
				else{
					if ((this->e[i][j] < 1.0 - err) && (this->e[i][j] > 1.0 + err)){
						return false;
					}
				}
			}
		}
		return true;
	}

	Matrix *  getTranspose(){

		int m = this->getColumns(); //numero de filas de matriz transpuesta
		int n = this->getRows(); // numero de columnas de matriz transpuesta
		Matrix *t = new Matrix(m, n);
		for (int i = 0; i < m; i++)
		{
			for (int j = 0; j < n; j++)
			{
				t->setElement(i, j, this->getElement(j, i));
			}
		}
		return t;
	}

	void setRoundRobin(int tam){
		int poten = this->nextPowOf2(tam);
		if (tam == (poten / 2)){
			poten /= 2;
		}
		this->m = poten + 1;
		this->n = poten;
		this->e = this->createMatrix(this->m, this->n);
		int delta = this->m - this->n;
		for (int i = 0; i < this->m; i++){
			for (int j = 0; j < this->n; j++){
				if (i == 0 || j == 0){
					if (i == 0){
						this->e[i][j] = j;
					}
					else{
						this->e[i][j] = i;
					}
				}
			}
		}
		for (int i = 1; i < this->m; i++){
			if (i % 2 != 0){
				this->e[i][1] = i + 1;
			}
			else if (i % 2 == 0){
				this->e[i][1] = i - 1;

			}
		}

		for (int j = 2; j < this->n; j = j * 2){

			int quarterY = this->nextPowOf2(j) / 2 + 1;
			for (int i = 1; i < this->m; i += j){

				//cout << "j: " << j << ", i:" << i << " Modificando casilla" << endl;

				int quarterX = this->nextPowOf2(j) / 2;
				int quarterhalfY = (this->nextPowOf2(i) - this->nextPowOf2(i) / 2) / 2;
				if (quarterhalfY == 0){
					quarterhalfY += 1;
				}
				if (i == quarterY){
					this->fillSection(i, j, (int)this->e[i - quarterX][0], (int)quarterX);
					quarterY += quarterX * 2;
				}
				else{
					this->fillSection(i, j, (int)this->e[i + quarterX][0], (int)quarterX);
				}
				//this->print();
				/*
				if (i > 1){
					getchar();
				}
				*/
			}


		}
	}
	void fillSection(int offsetY, int offsetX, int a, int rows){

		int curr = a;
		int b = a + rows - 1;
		for (int i = 0; i <rows; i++){
			for (int j = 0; j < rows; j++){
				this->e[offsetY + i][offsetX + j] = curr;
				if (j<rows - 1){
					curr++;
				}
				if (curr > b){
					curr = a;
				}
			}
		}
	}

	int nextPowOf2(int x){
		int num = 1;
		while (num <= x){
			num *= 2;
		}
		return num;
	}
	//Método invocado para Matrices ampliadas recibe la matriz sobre la que se guardará el arreglo de solución.
	//La operación se realiza sobre la matriz que contiene el objeto Matrix
	bool  resolverPorGaussJordan(Matrix *&sol){
		double CERO = 0.000001;
		bool haysol = false;
		double piv;
		double det = 1;
		Matrix *t = new Matrix(this);
		cout << "Usted ha introducido la siguiente matriz ampliada: " << endl;
		t->print();
		for (int i = 0; i < n - 1; i++)
		{
			piv = t->getElement(i, i);
			det = det*piv;
			if (piv == 0 || (abs(piv)) <= CERO){
				for (int k = 0; k < m; k++){
					if (t->e[k][i] != 0 || (abs(piv))>CERO){
						double *temp = t->e[k];
						t->e[k] = t->e[i];
						t->e[i] = temp;
					}
					cout << t->e[k][i];
				}
			}
			else if (piv != 1){
				for (int k = 0; k < n; k++){
					t->e[i][k] = t->e[i][k] / piv;
				}
			}
			for (int j = 0; j < m; j++)
			{
				if (abs(t->e[j][i]) > CERO && j != i){
					double reciproco = t->e[j][i] * (-1);
					for (int k = 0; k < n; k++){
						t->e[j][k] = t->e[i][k] * reciproco + t->e[j][k];
					}
				}
			}

		}
		cout << "Tabla Gauss Jordan resuelta" << endl;
		t->print();
		sol = new Matrix(1, m);
		for (int i = 0; i < m; i++){
			sol->e[0][i] = t->e[i][n - 1];
		}
		cout << "Fin" << endl;
		return haysol;
	}
private:
	double **  createMatrix(int m, int n)
	{
		double **a = new double *[m];
		for (int i = 0; i < m; i++)
		{
			a[i] = new double[n];
		}
		for (int i = 0; i < m; i++)
		{
			for (int j = 0; j < n; j++)
			{
				a[i][j] = 0.0;
			}
		}
		return a;
	}
};
