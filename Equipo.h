#pragma once
#include <iostream>
#include <string>

using namespace std;

class Equipo{
public:

	Equipo(){
		this->name = "New Team";
		this->abrev = "Team";
		this->weight = 0;
		this->wins = 0;
		this->loses = 0;
		this->ties = 0;
		this->gPlus = 0;
		this->gMinus = 0;
		this->position = 0;
	}

	Equipo(string nam, int wght){
		this->name = nam;
		this->weight = wght;
		this->abrev = "Team";
		this->wins = 0;
		this->loses = 0;
		this->ties = 0;
		this->gPlus = 0;
		this->gMinus = 0;
		this->position = 0;
	}
	void setName(string nam){
		this->name = nam;
	}
	int getWeight(){
		return this->weight;
	}
	int getPosition(){
		return this->position;
	}
	int getGoals(){
		return this->gPlus;
	}
	string getName(){
		return this->name;
	}

	string getAbrev(){
		return this->abrev;
	}

	void setAbrev(string abrev){
		this->abrev = abrev;
	}


	void updateWeight(int wght){
		this->weight = weight;
	}


	void print(){
		cout << this->getName() << endl << endl;
	}

	void printConPeso(){
		cout << this->getName() << ", " << this->getWeight() << endl << endl;
	}

	void printStats(){
		//cout << "Posicion No. " << this->getPosition() << endl;
		cout << this->getName() << endl;
		cout << "Juegos: " << this->getGames()<< endl;
		cout << "Ganados : " << this->wins << "   Perdidos: " << this->loses << "     Empatados: " << this->ties << endl;
		cout << "Goles a Favor:" << this->gPlus << " Goles en contra: " << this->gMinus << endl;
		cout << "Diferencia de goles: " << this->getGoalsDifference() << endl;
		cout << "Puntuaci�n Total: " << this->obtenerPuntuacion() << endl;
		cout << endl;
	}

	void setWins(int win){
		this->wins = win;
	}

	int getWins(){
		return this->wins;
	}

	void setLoses(int los){
		this->loses = los;
	}

	int getLoses(){
		return this->loses;
	}

	int getGoalsDifference(){
		return this->gPlus - this->gMinus;
	}

	void addWin(){
		this->wins++;
	}

	void addLose(){
		this->loses++;
	}

	void addGoal(){
		this->gPlus++;
	}

	void receiveGoal(){
		this->gMinus++;
	}

	void addGoals(int n){
		this->gPlus += n;
	}

	void recieveGoals(int n){
		this->gMinus += n;
	}

	void addTie(){
		this->ties++;
	}
	int getTies(){
		return this->ties;
	}
	int obtenerPuntuacion(){
		return this->wins * 3 + this->ties;
	}
	int getGames(){
		return this->getLoses() + this->getWins() + this->getTies();
	}
	int getGMinus(){
		return this->gMinus;
	}
private:
	int weight; //number from 0 to 100 indicating the team's chance of winning.
	int wins;
	int ties;
	int loses;
	int gPlus;
	int gMinus;
	string name;
	string abrev;
public:
	int position; //posic�n en el torneo
};
